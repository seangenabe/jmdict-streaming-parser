import { Duplex, DuplexOptions } from "stream"
import { createStream, SAXStream } from "sax"
import { parseGroupedEntities } from "./parse-grouped-entities"

export class JmdictTransform
  extends Duplex
  implements AsyncIterable<TransformOutputData> {
  parser: SAXStream
  entities = new Map<string, string>()
  private initialChunk = ""
  groupedEntities: ReadonlyMap<
    string,
    readonly { name: string; value: string }[]
  >
  private hasPassedOpeningTag = false
  inputStack: InputStartXml[] = []
  outputStack: OutputNode[] = [{}]

  constructor(opts: DuplexOptions = {}) {
    super({ ...opts, readableObjectMode: true })
    this.parser = createStream(true)

    // Proxy errors
    this.parser.on("error", (err) => this.emit("error", err))

    // Read entities
    this.parser.on("doctype", (doctype: string) => {
      const lines = doctype.split("\n")
      for (let line of lines) {
        line = line.trim()
        const matchResult = line.match(/^<!ENTITY\s+([^\s]+)\s+"([^"]+)">$/)
        if (matchResult) {
          const [, name, value] = matchResult

          // @ts-ignore
          this.parser._parser.ENTITIES[name] = name
          this.entities.set(name, value)
          this.push({ type: "entity", data: { name, value } })
        }
      }
    })

    this.parser.on(
      "opentag",
      ({ name, attributes }: { name: string; attributes: AttributeMap }) => {
        this.inputStack.push({ type: "start", name, attrs: attributes })

        // Push an empty object {} representing this element to the last object
        const lastEl = last(this.outputStack)!
        if (!lastEl[name]) {
          lastEl[name] = []
        }
        this.outputStack.push({})
      }
    )

    this.parser.on("closetag", (name: string) => {
      let xmlNode = this.inputStack.pop()!

      // Check end of root node
      if (this.inputStack.length === 0) {
        // End this stream
        this.push(null)
        return
      }

      const node = this.outputStack.pop()!

      // Merge attributes with output object
      Object.assign(node, xmlNode.attrs)

      if (Object.keys(node).length === 1 && "$text" in node) {
        ;(last(this.outputStack)![name] as string[]).push(node.$text as string)
      } else {
        // Check if this element is a child of the root node.
        if (this.inputStack.length === 1) {
          // Emit this element
          this.push({ type: "node", data: node })
        } else {
          ;(last(this.outputStack)![name] as OutputNode[]).push(node)
        }
      }
    })

    this.parser.on("text", (text: string) => {
      if (text === "\n") {
        return
      }
      last(this.outputStack)!.$text = text
    })

    this.parser.on("comment", (comment: string) => {
      comment = comment.trim()

      this.push({ type: "comment", data: comment })

      const mdateHint = "JMdict created: "
      if (comment.startsWith(mdateHint)) {
        const mdate = comment.slice(mdateHint.length)
        if (mdate) {
          this.push({ type: "mdate", data: mdate })
          this.push({ type: "entities", data: this.entities })
        }
      }
    })
  }

  _write(
    chunk: any,
    encoding: string,
    callback: (error?: Error | null) => void
  ): void {
    this.parser.write(chunk)
    if (!this.hasPassedOpeningTag) {
      this.initialChunk += chunk
      if (this.initialChunk.includes("\n<JMdict>\n")) {
        // Group entities
        this.hasPassedOpeningTag = true
        this.groupedEntities = parseGroupedEntities(this.initialChunk)
        this.push({ type: "groupedEntities", data: this.groupedEntities })
      }
    }
    callback()
  }

  _read(): void {}

  [Symbol.asyncIterator](): AsyncIterableIterator<TransformOutputData> {
    return super[Symbol.asyncIterator]()
  }
}

export interface EntityData {
  type: "entity"
  data: {
    name: string
    value: string
  }
}

export interface MdateData {
  type: "mdate"
  data: string
}

export interface NodeData {
  type: "node"
  data: OutputNode
}

export interface CommentData {
  type: "comment"
  data: string
}

export interface GroupedEntitiesData {
  type: "groupedEntities"
  data: JmdictTransform["groupedEntities"]
}

export type TransformOutputData =
  | EntityData
  | MdateData
  | NodeData
  | CommentData
  | GroupedEntitiesData

interface InputStartXml {
  type: "start"
  name: string
  attrs: AttributeMap
}

interface AttributeMap {
  [key: string]: string
}

function last<T>(arr: ReadonlyArray<T>): T | undefined {
  return arr[arr.length - 1]
}

export interface OutputNode {
  [key: string]: (string | OutputNode)[] | string
}
