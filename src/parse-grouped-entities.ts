export function parseGroupedEntities(initialChunk: string) {
  const lines = initialChunk.split("\n")
  let groupKey = ""
  const groupedEntities = new Map<string, { name: string; value: string }[]>()

  for (const line of lines) {
    if (line === "<JMdict>") {
      break
    }

    const groupHeaderMatch = /^<!--\s*?<(.*)>.*?entities\s*?-->$/.exec(line)

    if (groupHeaderMatch != null) {
      ;[, groupKey] = groupHeaderMatch
      groupedEntities.set(groupKey, [])
      continue
    }

    const entityDefinitionMatch = /^<!ENTITY\s+?([^\s]+)\s+?"([^"]+)/.exec(line)

    if (entityDefinitionMatch != null) {
      const [, name, value] = entityDefinitionMatch

      groupedEntities.get(groupKey)!.push({ name, value })
      continue
    }
  }

  return groupedEntities as ReadonlyMap<
    string,
    readonly { name: string; value: string }[]
  >
}
