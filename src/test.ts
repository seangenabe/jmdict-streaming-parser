import "source-map-support/register"
import { createGunzip } from "zlib"
import { createReadStream } from "fs"
import { JmdictTransform } from "."
import { join } from "path"
import { pipeline } from "stream"

const sourceStream = createReadStream(join(__dirname, "../files/JMdict_e.gz"))

const gunzip = createGunzip({})

const stream = new JmdictTransform({})

stream.on("data", () => {})

const start = process.hrtime.bigint()

pipeline(sourceStream, gunzip, stream, (err) => {
  if (err) {
    console.error(err)
    process.exit(1)
  }
  const end = process.hrtime.bigint()
  console.log(`parsing took ${end - start} ns (${end - start})`)
  console.log("end")
})
